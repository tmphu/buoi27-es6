import { Glasses } from "./model.js";

function createGlassObj(item) {
  let { id, src, virtualImg, brand, name, color, price, description } = item;
  let glassObj = new Glasses(
    id,
    src,
    virtualImg,
    brand,
    name,
    color,
    price,
    description
  );
  return glassObj;
}

export function createGlassArray(payload) {
  let Arr = [];
  payload.forEach((item) => {
    Arr.push(createGlassObj(item));
  });
  return Arr;
}

export function renderGlassesCollection(Arr) {
  let contentHTML = "";
  Arr.forEach((item, index) => {
    contentHTML += `
        <div class="col-4 vglasses__items" onclick="handleGlasses(${index})">
            <img src="${item.src}" alt="" />
        </div>
        `;
  });
  document.getElementById("vglassesList").innerHTML = contentHTML;
}
