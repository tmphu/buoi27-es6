import { renderGlassesCollection, createGlassArray } from "./controller.js";
import { dataGlasses } from "./model.js";

var glassArr = [];

glassArr = createGlassArray(dataGlasses);

renderGlassesCollection(glassArr);

function handleGlasses(index) {
  let glassDiv = document.getElementById("avatar");
  let item = glassArr[index];
  glassDiv.innerHTML = `
    <img src="${item.virtualImg}" alt="" />
    `;
  let glassInfo = document.getElementById("glassesInfo");
  glassInfo.style.display = "block";
  glassInfo.innerHTML = `
    <h5>${item.name} - ${item.brand} (${item.color})</h5>
    <p><span class="price">$${item.price}</span> <span class="note">Stocking</span></p>
    <p>${item.description}</p>
    `;
}
window.handleGlasses = handleGlasses;
